// C++ program to find intersection of
// two sorted arrays
// #include <bits/stdc++.h>
// using namespace std;
#include "main.h"
#include <stdint.h>
#include <stdio.h>



/* Function prints Intersection of arr1[] and arr2[]
m is the number of elements in arr1[]
n is the number of elements in arr2[] */

uint32_t coincStruct(photon *ph1, photon *ph2, uint64_t coinc[][1024], uint64_t coincOffset[][1024], uint64_t m, uint64_t n, int64_t offset)
{
    uint32_t cc = 0;

    if (offset>0)
    {
        int i = 0, j = 0;
        while (i < m && j < n)
        {
            if ((ph1[i].time+offset) < ph2[j].time)
                i++;
            else if (ph2[j].time < (ph1[i].time+offset))
                j++;
            else /* if arr1[i] == arr2[j] */
            {
                if (ph1[i].time > 0)
                {
                    if (offset == -1)
                        coinc[ph1[i].pixel][ph2[j].pixel]++;
                    else if (offset == 100-1)
                        coincOffset[ph1[i].pixel][ph2[j].pixel]++;
                    cc++;
                }
                i++;
                j++;
            }
        }
    }
    else
    {
        int i = 0, j = 0;
        while (i < m && j < n)
        {
            if (ph1[i].time < (ph2[j].time-offset))
                i++;
            else if ((ph2[j].time-offset) < ph1[i].time)
                j++;
            else /* if arr1[i] == arr2[j] */
            {
                if (ph1[i].time > 0)
                {
                    if (offset == -1)
                        coinc[ph1[i].pixel][ph2[j].pixel]++;
                    else if (offset == 100-1)
                        coincOffset[ph1[i].pixel][ph2[j].pixel]++;
                    cc++;
                }
                i++;
                j++;
            }
        }
    }
    return cc;
}

