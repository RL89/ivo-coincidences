/* To Be Implemented:
 *
 * Complete file readout, without neglecting the last few photons
 * Up/Down reorder
 *
 */
#include "main.h"
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <stdint.h>
#include <sys/stat.h>

#define debug 0
#define generate_coinc_files 0

int offsetFmin = -300;
int offsetFmax = 300;
int offsetTmin = -5;
int offsetTmax = 5;


int mainfunction(void);
int load_file(char *, uint16_t*, uint64_t*, uint16_t*, uint32_t*);
int load_config_file(int*, int*, int*, int*, char[][999], char[][999], int*);
int elab(uint16_t*, uint64_t*, uint16_t*, uint32_t*, uint64_t*, uint64_t*, uint16_t*, uint16_t*, photon*);
int hist(uint64_t*, uint16_t*, uint32_t*);
int sort(uint64_t*, uint32_t, photon*);
uint32_t coincStruct(photon*, photon*, uint64_t[][1024], uint64_t[][1024], uint64_t, uint64_t, int64_t);
int saveCSV(uint64_t[][1024], char*);

int compareStruct(const void * a, const void * b)
{
    photon *aa = (photon *)a;
    photon *bb = (photon *)b;

    if ((long long int)(aa->time) - (long long int)(bb->time) < 0)
        return -1;
    if ((long long int)(aa->time) - (long long int)(bb->time) > 0)
        return 1;
    return 0;
}

int main(void)
{
    int pippo = mainfunction();

    if (!pippo)
        printf("\nAll operations concluded succesfully\n");
    else
        printf("\nOperation concluded with error(s). Please check above.\n");
    //getchar();
    return pippo;
}



int mainfunction (void)
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    WORD saved_attributes;

    char filepathsA[99][999];
    char filepathsG[99][999];
    int numfiles = 0;

    if (load_config_file(&offsetFmin, &offsetFmax, &offsetTmin, &offsetTmax, filepathsA, filepathsG, &numfiles))
        return 1015;

    uint32_t *coincOffset = (uint32_t *)malloc((offsetFmax-offsetFmin+100)/100 * (offsetTmax-offsetTmin+1)*sizeof(uint32_t));
    for (int i=0; i < (offsetFmax-offsetFmin+100)/100 * (offsetTmax-offsetTmin+1); ++i)
        coincOffset[i] = 0;

    GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
    saved_attributes = consoleInfo.wAttributes;
    SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
    printf("   ###   Welcome to Coincidences v. %d.%d.%d!!!   ###   \n\n\n", ReleaseMaj, ReleaseMin, ReleaseBuild);
    SetConsoleTextAttribute(hConsole, saved_attributes);

    uint64_t coincidences[1024][1024], coincidencesOffset[1024][1024];
    for (uint32_t i=0; i<1024; ++i)
    {
        for (uint32_t j=0; j<1024; ++j)
        {
            coincidences[i][j] = 0;
            coincidencesOffset[i][j] = 0;
        }
    }


    for (int ipp=0; ipp<numfiles;++ipp)
    {
        SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
        printf("   ###   Iteration number %d   ###   \n\n\n", ipp);
        SetConsoleTextAttribute(hConsole, saved_attributes);

        uint16_t pixelA[2*BLOCKNO*BLOCKSIZE], pixelG[2*BLOCKNO*BLOCKSIZE], TOFA[2*BLOCKNO*BLOCKSIZE], TOFG[2*BLOCKNO*BLOCKSIZE];
        uint64_t frameA[2*BLOCKNO*BLOCKSIZE], frameG[2*BLOCKNO*BLOCKSIZE];

        uint64_t frmFiltA[2*BLOCKNO*BLOCKSIZE], frtFiltA[2*BLOCKNO*BLOCKSIZE], frmFiltG[2*BLOCKNO*BLOCKSIZE], frtFiltG[2*BLOCKNO*BLOCKSIZE];
        uint16_t tofFiltA[2*BLOCKNO*BLOCKSIZE], tofSimpA[2*BLOCKNO*BLOCKSIZE], tofFiltG[2*BLOCKNO*BLOCKSIZE], tofSimpG[2*BLOCKNO*BLOCKSIZE];

        photon photonA[2*BLOCKNO*BLOCKSIZE], photonG[2*BLOCKNO*BLOCKSIZE];

        for (uint64_t culo=0; culo<2*BLOCKNO*BLOCKSIZE; ++culo)
        {
            pixelA[culo]=0;
            pixelG[culo]=0;
            TOFA[culo]=0;
            TOFG[culo]=0;
            frameA[culo]=0;
            frameG[culo]=0;

            frmFiltA[culo]=0;
            frmFiltG[culo]=0;
            frtFiltA[culo]=0;
            frtFiltG[culo]=0;
            photonA[culo].time=0;
            photonG[culo].time=0;
            photonA[culo].pixel=9999;
            photonG[culo].pixel=9999;

            tofFiltA[culo]=0;
            tofFiltG[culo]=0;
            tofSimpA[culo]=0;
            tofSimpG[culo]=0;
        }

        uint32_t validA=0, validG=0;

        char *filepath;
        filepath = filepathsA[ipp];
        if (verbose)
            printf(" >> Reading data from file: %s.\nPlease wait . . . \n\n", filepath);
        if (load_file(filepath, pixelA, frameA, TOFA, &validA))
            return 1001;
        if (verbose)
            printf("Done.\n");

        filepath = filepathsG[ipp];

        if (verbose)
            printf(" >> Reading data from file: %s.\nPlease wait . . . \n\n", filepath);
        if (load_file(filepath, pixelG, frameG, TOFG, &validG))
            return 1002;
        if (verbose)
            printf("Done.\n");

        if (verbose)
            printf(" >> Elaborating Achiropita.\nPlease wait . . . \n\n");
        elab(pixelA, frameA, TOFA, &validA, frmFiltA, frtFiltA, tofFiltA, tofSimpA, photonA);
        if (verbose)
            printf("Done.\n");
        if (verbose)
            printf(" >> Elaborating Genoveffa.\nPlease wait . . . \n\n");
        elab(pixelG, frameG, TOFG, &validG, frmFiltG, frtFiltG, tofFiltG, tofSimpG, photonG);
        if (verbose)
            printf("Done.\n");

        if (verbose)
            printf(" >> Sorting Achiropita.\nPlease wait . . . \n\n");
        sort(frtFiltA, validA, photonA);
        if (verbose)
            printf("Done.\n");
        if (verbose)
            printf(" >> Sorting Genoveffa.\nPlease wait . . . \n\n");
        sort(frtFiltG, validG, photonG);
        if (verbose)
            printf("Done.\n");

        if (verbose)
            printf(" >> Checking coincidences.\nPlease wait . . . \n\n");
        int offidx = 0;
        for (int64_t offsetF = offsetFmin;offsetF<=offsetFmax; offsetF+=100)
        {
            for (int64_t offsetT = offsetTmin;offsetT<=offsetTmax;++offsetT)
            {
                coincOffset[offidx] += coincStruct(photonA, photonG, coincidences, coincidencesOffset, validA, validG, offsetF+offsetT);
                offidx++;

                if (offsetF + offsetT == 100-1)
                {
                    printf("Generating output %d\n", offsetF+offsetT);
                    if (saveCSV(coincidencesOffset, "outputOffset.csv"))
                    {
                        printf("Could not create file outputOffset.csv\n");
                        return 5;
                    }
                }
                if (offsetF + offsetT == -1)
                {
                    printf("Generating output %d\n", offsetF+offsetT);
                    if (saveCSV(coincidences, "output.csv"))
                    {
                        printf("Could not create file output.csv\n");
                        return 6;
                    }
                }
            }
        }

        if (verbose)
            printf("Done.\n");
        if (verbose)
            printf("VALIDS: %lli, %lli\n", validA, validG);

        if (generate_coinc_files)
        {
            if (verbose)
                printf(" >> Generating coincidence files\nPlease wait . . . \n\n");

            for (uint64_t offsetF = 0;offsetF<=600; offsetF+=100)
            {

                for (uint64_t offsetT=0;offsetT<=10;++offsetT)
                {
                    char filename[50];
                    uint32_t coinc = 0;
                    sprintf(filename, "Offset frame %d, pulse %d.txt",((int)offsetF-300)/100, (int)offsetT-5);
                    FILE *fileid = fopen(filename, "w");
                    uint64_t frtFiltAs[2*BLOCKNO*BLOCKSIZE];

                        for (uint64_t i=0;i<validA;++i)
                        {
                            if (frtFiltA[i]>0)
                                frtFiltAs[i] = frtFiltA[i]+offsetF+offsetT-300-5;
                            else
                                frtFiltAs[i] = 0;
                        }

                        uint64_t i = 0, j = 0;
                        while (frtFiltAs[i]<12000000000)
                            i++;
                        while (frtFiltG[j]<12000000000)
                            j++;
                        while (i < validA && j < validG && frtFiltAs[i]<19000000000 && frtFiltG[j]<19000000000) // 56 * 70000000 = 3.92*10^9
                        {
                            if (frtFiltAs[i] < frtFiltG[j])
                            {
                                i++;
                                if (frtFiltAs[i] > 0)
                                {
                                    fprintf(fileid, "1,0\n");
                                }
                            }
                            else if (frtFiltG[j] < frtFiltAs[i])
                            {
                                j++;
                                if (frtFiltG > 0)
                                {
                                    fprintf(fileid, "0,1\n");
                                }
                            }
                            else /* if arr1[i] == arr2[j] */
                            {
                                if (frtFiltAs[i] > 0 && frtFiltG > 0)
                                {
                                    fprintf(fileid, "1,1\n");
                                    coinc++;
                                }
                                i++;
                                j++;
                            }
                        }

                    if (verbose)
                        printf("Offset: %lli -> coincidences: %lli\n",offsetT+offsetF-305, coinc);
                    fclose(fileid);
                }
            }
        }
        if (verbose)
            printf("Done.\n");

    }


    int offidx = 0;
    for (int64_t offsetF = offsetFmin;offsetF<=offsetFmax; offsetF+=100)
    {
        for (int64_t offsetT = offsetTmin;offsetT<=offsetTmax;++offsetT)
        {
            printf("Offset: %lli -> Coinc: %lli\n", offsetT+offsetF, coincOffset[offidx]);
            ++offidx;
        }
    }

    return 0;

}


int sort(uint64_t *frt, uint32_t valid, photon *ph)
{

    if (debug)
    {
        FILE *fileid = fopen("UNSORTEDTOF.txt", "w");
        for (uint32_t i=0; i<valid; ++i)
        {
            fprintf(fileid,"%lli\n", frt[i]);
        }
        fclose(fileid);
    }
    if (verbose)
        printf("Sorting %d elements\n", valid);
    qsort(ph, valid, sizeof(photon), compareStruct);

    if (debug)
    {
        FILE *fileid = fopen("SORTEDTOF.txt", "w");
        for (uint32_t i=0; i<valid; ++i)
        {
            fprintf(fileid,"%lli\n", frt[i]);
        }
        fclose(fileid);
    }

    return 0;
}



int elab(uint16_t *pixel, uint64_t *frame, uint16_t *tof, uint32_t *valid, uint64_t *frmFilt, uint64_t *frtFilt, uint16_t *tofFilt, uint16_t *tofSimp, photon *ph)
{
    FILE *fileid;
    if (debug)
        fileid = fopen("FILTEREDTOF.txt", "w");

    uint64_t isto[1024];
    for (int i=0;i<1024;++i)
        isto[i] = 0;

    unsigned short offpeak = 0, peakwidth = 1;

    if (hist(isto, tof, valid))
        return 1;

    int offset = 20;
    int width = 20;
    int j = 0, ptr = 0;

    while (offset < 1000 - width -50)
    {
        ++j;
        int maxind = 0;
        for (int i=offset; i<=offset+width; ++i)
            if (isto[i] > isto[maxind])
                maxind = i;

        for (uint32_t i=0; i < *valid; ++i)
        {
            if (tof[i] >= maxind-peakwidth+offpeak && tof[i] <= maxind+peakwidth+offpeak)
            {
                tofFilt[ptr] = tof[i];
                tofSimp[ptr] = j;
                frmFilt[ptr] = frame[i];
                frtFilt[ptr] = frame[i]*100 + j;
                ph[ptr].time = frame[i]*100 + j;
                ph[ptr].pixel = pixel[i];
                if (debug)
                    fprintf(fileid, "%lli\n", tofFilt[ptr]);
                ++ptr;
            }
        }

        offset = maxind + width/2;
    }
    if (debug)
        fclose(fileid);

    return 0;
}


int hist(uint64_t *histogram, uint16_t *tof, uint32_t *valid)
{
    for (int i=0; i<1024; ++i)
        histogram[i] = 0;
    for (uint32_t i = 0; i < *valid; ++i)
    {
        if (tof[i] > 1023)
        {
            printf("Warning! \"TOF>1023\" %lli %d\n", i, tof[i]);
            getchar();
        }
        histogram[tof[i]]++;
    }

    if (verbose)
        printf("Histogram generated.\n");
    if (debug)
    {
        FILE *fileid = fopen("HISTOGRAM.txt", "w");
        for (int i = 0; i < 1024; ++i)
            fprintf(fileid, "%lli\n", histogram[i]);
        fclose(fileid);
    }
    return 0;
}
