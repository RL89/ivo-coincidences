clear all;

folder = cd;
cd 'D:\GIT\SW_IvoExp2\build-Coincidences-Desktop_Qt_5_2_1_MSVC2012_OpenGL_64bit-Release\release';

[fileA folderA] = uigetfile('*Achiropita.cprdat','MultiSelect','on', ['Please select all ACHIROPITA files. ', ...
    '(Suggestion: you should select more than one file for acquisitions longer than 15 min)']);
[fileG folderG] = uigetfile([folderA,'\*Genoveffa.cprdat'],'MultiSelect','on', ['Please select all GENOVEFFA files. ', ...
    '(Suggestion: you should select more than one file for acquisitions longer than 15 min)']);


fileid = fopen('config.txt', 'w');
fwrite(fileid, 'Offset Frame, Minimum: -2');
fprintf(fileid, '\n');
fwrite(fileid, 'Offset Frame, Maximum: 2');
fprintf(fileid, '\n');
fwrite(fileid, 'Offset Pulse, Minimum: -3');
fprintf(fileid, '\n');
fwrite(fileid, 'Offset Pulse, Maximum: 3');
fprintf(fileid, '\n');

fwrite(fileid, 'Files Achiropita');
fprintf(fileid, '\n');
if iscell(fileA)
    for i=1:length(fileA)
        fwrite(fileid, [folderA, cell2mat(fileA(i))]);
        fprintf(fileid, '\n');
    end
else % Matlab is a whore
    fwrite(fileid, [folderA, fileA]);
    fprintf(fileid, '\n');
end

fwrite(fileid, 'Files Genoveffa');
fprintf(fileid, '\n');
if iscell(fileG)
    for i=1:length(fileG)
        fwrite(fileid, [folderG, cell2mat(fileG(i))]);
        fprintf(fileid, '\n');
    end
else % Matlab is a whore
    fwrite(fileid, [folderG, fileG]);
    fprintf(fileid, '\n');
end

fwrite(fileid, 'End');
fprintf(fileid, '\n');
fclose(fileid);

if system('Coincidences')
    cd(folder)
    return;
end
cc = importdata('output.csv');
ccoffset = importdata('outputOffset.csv');

cd(folder)


figure(1242)
subplot(1,3,1);
im1 = imagesc(reshape(sum(cc,2),32,32));
axis image;
hold on;
hm1 = plot(-3, -3, 'r+', 'MarkerSize', 5, 'LineWidth', 3);
xlim([0.5 32.5]);
colorbar;
set(im1,'ButtonDownFcn','cbc(im1, 1, cc, hm1)')
title('Achiropita coincidences')

subplot(1,3,2);
im2 = imagesc(reshape(sum(cc,1),32,32));
axis image;
hold on;
hm2 = plot(-3, -3, 'r+', 'MarkerSize', 5, 'LineWidth', 3);
xlim([0.5 32.5]);
colorbar;
set(im2,'ButtonDownFcn','cbc(im2, 2, cc, hm2)')
title('Genoveffa coincidences')

helpstr = ['Left and middle figures: coincidences detected for Achiropita and Genoveffa, respectively, ', ...
    'accumulated across all the laser shots and frames of the acquisition. Click on any ', ...
    'of the pixels of one of the first two figures to open the right figure, which shows ', ...
    'the coincidences associated to the selected pixel. For instance, click on pixel (12,15) ', ...
    'on the first figure to display all the detections on Genoveffa associated to Achiropita''s ', ...
    'pixel (12,15).'];

hu = uicontrol('Style', 'pushbutton', 'FontSize', 24, 'String', '* HELP *', 'Callback','msgbox(helpstr)');
set(hu,'Position',[20 20 240 80]);
ht = uicontrol('Style', 'text', 'FontSize', 24, ...
    'String', {['Total number of coincidences: ', num2str(sum(sum(cc)))], ...
    ['Accidental coincidences: ', num2str(sum(sum(ccoffset)))]});
set(ht,'Position',[260 16 600 80]);
