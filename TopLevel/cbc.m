function cbc(handle, winnum, cc, hm)


figure(1242);
pos = round(get(gca,'CurrentPoint'));
x = pos(1)-1;
y = pos(3)-1;


subplot(1,3,3)
if winnum == 1
    pd = x*32+y+1;
    imm = imagesc(reshape(cc(pd,:),32,32));
    title({'Coincidences for Genoveffa','associated to Achiropita''s',['pixel ',num2str(x),',',num2str(y)]});

else
    pd = x*32+y+1;
    imm = imagesc(reshape(cc(:,pd),32,32));
    title({'Coincidences for Achiropita','associated to Genoveffa''s',['pixel ',num2str(x),',',num2str(y)]});

end

axis image
colorbar
set(hm,'XData',x+1);
set(hm,'YData',y+1);

set(imm,'ButtonDownFcn','beep;disp(''You are not supposed to click here!'')')

end

