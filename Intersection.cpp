// C++ program to find intersection of
// two sorted arrays
// #include <bits/stdc++.h>
// using namespace std;
#include <stdint.h>
#include <stdio.h>



/* Function prints Intersection of arr1[] and arr2[]
m is the number of elements in arr1[]
n is the number of elements in arr2[] */
uint32_t coinc(uint64_t *arr1, uint64_t *arr2, uint64_t *coinc, uint64_t m, uint64_t n, int64_t offset)
{
    uint32_t cc = 0;
    if (offset>0)
    {
        int i = 0, j = 0;
        while (i < m && j < n)
        {
            if ((arr1[i]+offset) < arr2[j])
                i++;
            else if (arr2[j] < (arr1[i]+offset))
                j++;
            else /* if arr1[i] == arr2[j] */
            {
                //       cout << arr2[j] << " ";
                if (arr1[i] > 0)
                {
                    cc++;
                    //printf("%lli %lli %lli\n",arr1[i], arr2[j], cc);
                }
                i++;
                j++;
            }
        }
    }
    else
    {
        int i = 0, j = 0;
        while (i < m && j < n)
        {
            if (arr1[i] < (arr2[j]-offset))
                i++;
            else if ((arr2[j]-offset) < arr1[i])
                j++;
            else /* if arr1[i] == arr2[j] */
            {
                //       cout << arr2[j] << " ";
                if (arr1[i] > 0)
                {
                    cc++;
                    //printf("%lli %lli %lli\n",arr1[i], arr2[j], cc);
                }
                i++;
                j++;
            }
        }
    }
    return cc;
}
