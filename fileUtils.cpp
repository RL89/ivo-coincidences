#include <stdio.h>
#include <stdint.h>
#include <string.h>

int load_config_file(int *offsetFmin, int *offsetFmax, int *offsetTmin, int *offsetTmax, char filepathsA[][999], char filepathsG[][999], int *numfiles)
{
    char readstr[999];
    FILE *fileid = fopen("config.txt", "rt");
    if (fileid==NULL)
    {
        printf("ERROR: Config file could not be loaded.\n");
        return 1;
    }
    fscanf(fileid, "Offset Frame, Minimum: %d\n",offsetFmin);
    fscanf(fileid, "Offset Frame, Maximum: %d\n",offsetFmax);
    fscanf(fileid, "Offset Pulse, Minimum: %d\n",offsetTmin);
    fscanf(fileid, "Offset Pulse, Maximum: %d\n",offsetTmax);

    fgets(readstr, 999, fileid);
    if (!(strcmp(readstr, "Files Achiropita")))
    {
        printf("ERROR: Unexpected string in config file.\n%s\n", readstr);
        return 2;
    }

    int i = 0;
    for (fgets(readstr, 999, fileid);(strcmp(readstr, "Files Genoveffa\n"));fgets(readstr, 999, fileid))
    {
        size_t ln = strlen(readstr) - 1;
        if (*readstr && readstr[ln] == '\n')
            readstr[ln] = '\0';
        if (strcmp(readstr, "Files Genoveffa\n"))
            strcpy(filepathsA[i], readstr);
        ++i;
    }

    int j = 0;
    for (fgets(readstr, 999, fileid);(strcmp(readstr, "End\n"));fgets(readstr, 999, fileid))
    {
        size_t ln = strlen(readstr) - 1;
        if (*readstr && readstr[ln] == '\n')
            readstr[ln] = '\0';
        if (strcmp(readstr, "End\n"))
            strcpy(filepathsG[j], readstr);
        ++j;
    }

    if (i != j)
    {
        printf("ERROR: Achiropita and Genoveffa must have the same number of files (%d files specified for Achiropita, %d for Genoveffa)!\n", i, j);
        return 3;
    }
    *numfiles = i;

    *offsetFmax *= 100;
    *offsetFmin *= 100;

    if (verbose)
        printf("Offsets: %d %d %d %d\n", *offsetFmin, *offsetFmax, *offsetTmin, *offsetTmax);
    if (feof(fileid))
    {
        printf("ERROR: Unexpected end of config file.\n");
        return 2;
    }

    fclose(fileid);
    return 0;
}




int load_file(char *filepath, uint16_t *pixel, uint64_t *frame, uint16_t *tof, uint32_t *valid)
{
    uint64_t lastFrame = 0;
    uint32_t data[BLOCKNO*BLOCKSIZE];
    uint64_t frames[2*BLOCKNO*BLOCKSIZE];
    uint32_t coarseup[BLOCKNO*BLOCKSIZE], coarsedw[BLOCKNO*BLOCKSIZE], fineup[BLOCKNO*BLOCKSIZE], finedw[BLOCKNO*BLOCKSIZE], pixelup[BLOCKNO*BLOCKSIZE], pixeldw[BLOCKNO*BLOCKSIZE], stops[2*BLOCKNO*BLOCKSIZE], tofup[BLOCKNO*BLOCKSIZE], tofdw[BLOCKNO*BLOCKSIZE];
    for (uint64_t i = 0; i<BLOCKNO*BLOCKSIZE;++i)
    {
        data[i]=0;
        coarseup[i]=0;
        coarsedw[i]=0;
        fineup[i]=0;
        finedw[i]=0;
        pixelup[i]=0;
        pixeldw[i]=0;
        tofup[i]=0;
        tofdw[i]=0;
    }
    for (uint64_t i = 0; i<2*BLOCKNO*BLOCKSIZE;++i)
    {
        frames[i]=0;
        stops[i]=0;
    }
    char dataH[1024];

    FILE *fileId = fopen(filepath, "rb");

    if (fileId==NULL)
    {
        printf("ERROR: File could not be loaded.\n");
        return 1;
    }

    fread(dataH, 1024, sizeof(char), fileId);	// File header, not interestings

    int idvec=0;
    for (int index = 0; index < BLOCKNO && !(feof(fileId)); ++index)
    {

        fread(data+index*BLOCKSIZE, BLOCKSIZE, sizeof(uint32_t), fileId);
        if (feof(fileId))
            break;
    }
    fclose(fileId);

    for (int i=0; i<BLOCKNO*BLOCKSIZE; ++i)
    {
        pixelup[i] = 10000;
        pixeldw[i] = 10000;
        coarseup[i] = 1000;
        coarsedw[i] = 1000;
        fineup[i] = 0;
        finedw[i] = 0;
        frames[i] = 0;
        stops[i] = 0;

        data[i] = ((data[i]&0xFFFF0000)>>16) + ((data[i]&0x0000FFFF)<<16);	// Reorder
        if (data[i] & 0x80000000)												// if footer
        {
            frames[i] = (uint64_t)((data[i] & 0x7FFFFFF0) >> 4);
            while (frames[i]<lastFrame)
                frames[i] += 0x8000000;
            lastFrame = frames[i];
            stops[i] = (data[i] & 0x0000000F);
        }
        else																	// if data
        {

            if (data[i] & 0x40000000)											// valid up
            {
                pixelup[i] = (data[i] & 0x1FF00000) >> 20;
                coarseup[i] = (data[i] & 0x0000FC00) >> 10;
                fineup[i] = (data[i] & 0x000F0000) >> 16;
            }
            if (data[i] & 0x20000000)											// valid dw
            {

                pixeldw[i] = ((data[i] & 0x1FF00000) >> 20);
                pixeldw[i] = (pixeldw[i] & 0x1F) | (0x1E0 & ~pixeldw[i]);
                pixeldw[i] = pixeldw[i] + 512;
                coarsedw[i] = (data[i] & 0x0000003F);
                finedw[i] = (data[i] & 0x000003C0) >> 6;
            }
        }
    }


    short cut = 1;
    for (int i=BLOCKNO*BLOCKSIZE; i>=0; --i)
    {
        if (cut)
        {
            if (frames[i]!=0)
                cut = 0;
        }
        else
        {
            if (frames[i] == 0)
            {
                frames[i] = frames[i+1];
                stops[i] = stops[i+1];

                if (coarseup[i] != 1000 && coarseup[i] > 0)
                    tofup[i] = (coarseup[i]<<4) + stops[i] - fineup[i];
                if (coarsedw[i] != 1000 && coarsedw[i] > 0)
                    tofdw[i] = (coarsedw[i]<<4) + stops[i] - finedw[i];

                if (tofup[i]>1023 && coarseup[i]!=1000)
                    printf("UP %d %d %d %d %d\n", pixelup[i], coarseup[i], fineup[i], stops[i], tofup[i]);
                if (tofdw[i]>1023 && coarsedw[i]!=1000)
                    printf("DW %d %d %d %d %d\n", pixeldw[i], coarsedw[i], finedw[i], stops[i], tofdw[i]);
            }
            else
            {
                pixelup[i] = 10000;
                pixeldw[i] = 10000;
            }
        }
    }

    for (int j=0; j<BLOCKNO*BLOCKSIZE; ++j)
    {
        if (pixelup[j]<10000)
        {
            pixel[idvec] = (uint16_t)pixelup[j];
            frame[idvec] = frames[j];
            tof[idvec] = (uint16_t)(tofup[j]);
            ++idvec;
        }
    }

    for (int j=0; j<BLOCKNO*BLOCKSIZE; ++j)
    {
        if (pixeldw[j]<10000)
        {
            pixel[idvec] = (uint16_t)pixeldw[j];
            frame[idvec] = frames[j];
            tof[idvec] = (uint16_t)tofdw[j];
            ++idvec;
        }
    }


    *valid = idvec;
    if (verbose)
        printf("Index is %d\n", idvec);


    return 0;
}



int saveCSV(uint64_t coinc[][1024], char *filename)
{
    FILE *fileid = fopen(filename, "wt");
    if (fileid != NULL)
    {
        for (int i=0;i<1024;++i)
        {
            for (int j=0;j<1024;++j)
                fprintf(fileid, "%lli;", coinc[i][j]);
            fprintf(fileid, "\n");
        }
    }
    else
        return 5;
    fclose(fileid);
    return 0;
}
